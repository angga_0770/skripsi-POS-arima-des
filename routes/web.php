<?php

use App\Http\Controllers\{
    ArimaController,
    DesController,
    DashboardController,
    KategoriController,
    LaporanController,
    ProdukController,
    MemberController,
    PengeluaranController,
    PembelianController,
    PembelianDetailController,
    PenjualanController,
    PenjualanDetailController,
    SettingController,
    SupplierController,
    UserController,
    ForecastController
};
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Stmt\Foreach_;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/template', function () {
    return view('layouts.master-flat-ui');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::group(['middleware' => 'level:1'], function () {
        Route::get('/kategori/data', [KategoriController::class, 'data'])->name('kategori.data');
        Route::resource('/kategori', KategoriController::class);

        Route::get('/produk/data', [ProdukController::class, 'data'])->name('produk.data');
        Route::post('/produk/delete-selected', [ProdukController::class, 'deleteSelected'])->name('produk.delete_selected');
        Route::post('/produk/cetak-barcode', [ProdukController::class, 'cetakBarcode'])->name('produk.cetak_barcode');
        // Route::post('/produk/store', [ProdukController::class, 'store'])->name('produk.store');
        Route::resource('/produk', ProdukController::class);

        Route::get('/member/data', [MemberController::class, 'data'])->name('member.data');
        Route::post('/member/cetak-member', [MemberController::class, 'cetakMember'])->name('member.cetak_member');
        Route::resource('/member', MemberController::class);

        Route::get('/supplier/data', [SupplierController::class, 'data'])->name('supplier.data');
        Route::resource('/supplier', SupplierController::class);

        Route::get('/pengeluaran/data', [PengeluaranController::class, 'data'])->name('pengeluaran.data');
        Route::resource('/pengeluaran', PengeluaranController::class);

        Route::get('/pembelian/data', [PembelianController::class, 'data'])->name('pembelian.data');
        Route::get('/pembelian/{id}/create', [PembelianController::class, 'create'])->name('pembelian.create');
        Route::resource('/pembelian', PembelianController::class)
            ->except('create');

        Route::get('/pembelian_detail/{id}/data', [PembelianDetailController::class, 'data'])->name('pembelian_detail.data');
        Route::get('/pembelian_detail/loadform/{diskon}/{total}', [PembelianDetailController::class, 'loadForm'])->name('pembelian_detail.load_form');
        Route::resource('/pembelian_detail', PembelianDetailController::class)
            ->except('create', 'show', 'edit');

        Route::get('/penjualan/data', [PenjualanController::class, 'data'])->name('penjualan.data');
        Route::get('/penjualan', [PenjualanController::class, 'index'])->name('penjualan.index');
        Route::get('/penjualan/{id}', [PenjualanController::class, 'show'])->name('penjualan.show');
        Route::delete('/penjualan/{id}', [PenjualanController::class, 'destroy'])->name('penjualan.destroy');

        //Forecasting
        Route::post('/arima/forecast',[ArimaController::class, 'forecast'])->name('arima.forecast');
        Route::post('/arima/forecastModel',[ArimaController::class, 'store']);
        Route::resource('/arima', ArimaController::class);

        Route::post('/des/forecast',[DesController::class, 'forecast']);
        Route::post('/des/forecastModel',[DesController::class, 'store']);
        Route::post('/des/forecast',[DesController::class, 'forecast']);
        Route::resource('/des', DesController::class);

        Route::resource('/implementation', ForecastController::class);
        Route::post('/implementation/forecast', [ForecastController::class, 'forecast']);
        Route::get('/implementation/implementation', [ForecastController::class, 'implementationIndex'])->name('implementation.implementation');
        Route::post('/implementation/implementation', [ForecastController::class, 'implementation']);
    });

    Route::group(['middleware' => 'level:1,2'], function () {
        Route::get('/transaksi/baru', [PenjualanController::class, 'create'])->name('transaksi.baru');
        Route::post('/transaksi/simpan', [PenjualanController::class, 'store'])->name('transaksi.simpan');
        Route::get('/transaksi/selesai', [PenjualanController::class, 'selesai'])->name('transaksi.selesai');
        Route::get('/transaksi/nota-kecil', [PenjualanController::class, 'notaKecil'])->name('transaksi.nota_kecil');
        Route::get('/transaksi/nota-besar', [PenjualanController::class, 'notaBesar'])->name('transaksi.nota_besar');

        Route::get('/transaksi/{id}/data', [PenjualanDetailController::class, 'data'])->name('transaksi.data');
        Route::get('/transaksi/loadform/{diskon}/{total}/{diterima}', [PenjualanDetailController::class, 'loadForm'])->name('transaksi.load_form');
        Route::resource('/transaksi', PenjualanDetailController::class)
            ->except('create', 'show', 'edit');
    });

    Route::group(['middleware' => 'level:1'], function () {
        Route::get('/laporan', [LaporanController::class, 'index'])->name('laporan.index');
        Route::get('/laporan/data/{awal}/{akhir}', [LaporanController::class, 'data'])->name('laporan.data');
        Route::get('/laporan/pdf/{awal}/{akhir}', [LaporanController::class, 'exportPDF'])->name('laporan.export_pdf');

        Route::get('/user/data', [UserController::class, 'data'])->name('user.data');
        Route::resource('/user', UserController::class);

        Route::get('/setting', [SettingController::class, 'index'])->name('setting.index');
        Route::get('/setting/first', [SettingController::class, 'show'])->name('setting.show');
        Route::post('/setting', [SettingController::class, 'update'])->name('setting.update');
    });

    Route::group(['middleware' => 'level:1,2'], function () {
        Route::get('/profil', [UserController::class, 'profil'])->name('user.profil');
        Route::post('/profil', [UserController::class, 'updateProfil'])->name('user.update_profil');
    });

    Route::get('/file-import-kategori',[KategoriController::class,'importView'])->name('import-view-kategori');
    Route::post('/import-kategori',[KategoriController::class,'import'])->name('import-kategori');

    Route::get('/file-import-produk',[ProdukController::class,'importView'])->name('import-view-produk');
    Route::post('/import-produk',[ProdukController::class,'import'])->name('import-produk');

    Route::get('/file-import-member',[MemberController::class,'importView'])->name('import-view-member');
    Route::post('/import-member',[MemberController::class,'import'])->name('import-member');

    Route::get('/file-import-supplier',[SupplierController::class,'importView'])->name('import-view-supplier');
    Route::post('/import-supplier',[SupplierController::class,'import'])->name('import-supplier');

    Route::get('/file-import-penjualan',[PenjualanController::class,'importView'])->name('import-view-penjualan');
    Route::post('/import-penjualan',[PenjualanController::class,'import'])->name('import-penjualan');

    Route::get('/file-import-penjualanDetail',[PenjualanDetailController::class,'importView'])->name('import-view-penjualanDetail');
    Route::post('/import-penjualanDetail',[PenjualanDetailController::class,'import'])->name('import-penjualanDetail');


    Route::get('/generateAllModel', [ForecastController::class, 'AllModel']);
});
