<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForecastModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forecast_model', function (Blueprint $table) {
            $table->id();
            $table->integer('id_barang');
            $table->integer('ar')->nullable();
            $table->integer('diff')->nullable();
            $table->integer('ma')->nullable();
            $table->float('mu')->nullable();
            $table->float('lambda')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecast_model');
    }
}
