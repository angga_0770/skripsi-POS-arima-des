<?php

namespace App\Http\Controllers;

use App\Models\ForecastModel;
use App\Models\Produk;
use App\ARIMA\arimaModel;
use App\Models\Penjualan;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\PenjualanDetail;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class ArimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Produk::all();
        return view('arima.index', [
            'products' => $products
        ]);
    }

    public function forecast(Request $request)
    {
        //Ambil Semua data
        $orders = PenjualanDetail::select(
                    "id_produk",
                    DB::raw("(sum(jumlah)) as total_jumlah"),
                    DB::raw("(WEEK(created_at)) as perminggu"),
                    "created_at"
                )
                ->where('id_produk', $request->id_produk)
                ->orderBy('created_at')
                ->groupBy('perminggu')
                ->get();

        //Menghitung semua data
        $totalRow = count($orders);

        //Konstanta 80 persen
        $eightyPercent = ceil(0.8 * $totalRow);

        //Mengambil 80% data => dari awal
        $dataTraining = PenjualanDetail::select(
                            "id_produk",
                            DB::raw("(sum(jumlah)) as total_jumlah"),
                            DB::raw("(WEEK(created_at)) as perminggu"),
                            "created_at")
                        ->where('id_produk', $request->id_produk)
                        ->orderBy('created_at')
                        ->groupBy('perminggu')
                        ->take($eightyPercent)
                        ->pluck('total_jumlah')
                        ->toArray();

        //Mengambil 20% data - Terakhir
        $dataTesting = PenjualanDetail::select(
                            "id_produk",
                            DB::raw("(sum(jumlah)) as total_jumlah"),
                            DB::raw("(WEEK(created_at)) as perminggu"),
                            "created_at")
                        ->where('id_produk', $request->id_produk)
                        ->orderBy('created_at')
                        ->groupBy('perminggu')
                        ->offset($eightyPercent)
                        ->limit($totalRow - $eightyPercent)
                        ->pluck('total_jumlah')
                        ->toArray();

        $countDataTesting = count($dataTesting);

        $datePerWeeks = [];
        foreach ($orders as $order) {
            $created_at = Carbon::parse($order->created_at);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //Date untuk Peramalan data testing
        $dateDataTesting = [];
        foreach (array_reverse($datePerWeeks) as $key => $dateValue) {
            array_push($dateDataTesting, $dateValue);
            if ($key == $countDataTesting - 1) {
               break;
            }
        }

        //--------------------------Awal ARIMA----------------------
            $dataArima = $dataTraining;
            $orderArima = NULL;

            //Cek apakah menggunakan auto Arima atau tidak
            if ($request->autoArima == NULL) {
                $orderArima = [
                    $request->ar,
                    $request->diff,
                    $request->ma,
                    $request->id_produk
                ];
                $res_arima = arimaModel::arima($dataArima, $orderArima, $countDataTesting);
            } else {
                //Auto ARIMA
                $res_arima = arimaModel::auto_arima($dataArima, $pred_num = $countDataTesting, $algo='AIC');
                $orderArima = arimaModel::get_auto_arima_order();
            }
        //-------------------------Akhir Arima--------------

        //Menghitung Nilai Mape
            $sumMape = 0;
            for ($i=0; $i < $countDataTesting; $i++) {
                //Mape
                $sumMape += abs(($dataTesting[$i] - $res_arima[$i]) / $dataTesting[$i]);

                //Symmetric_mean_absolute_percentage_error
                //hasil balikannya ($sumMape / $countDataTesting) * 100
                // $sumMape += abs($dataTesting[$i] - $res_arima[$i]) / ((abs($dataTesting[$i]) - abs($res_arima[$i])) / 2);
            }

            $mape = round((100 / $countDataTesting) * $sumMape, 0) / 100 ;

//             if ($mape > 100) {
//                 $penguranganMape = rand(51,70);
//             } elseif ($mape < 100 || $mape > 50){
//                 $penguranganMape = rand(30,50);
//             } else {
//                 $penguranganMape = $mape;
//             }

        return view('arima.forecast', [
            'orders' => $orders,
            'forcastArima' => $res_arima,
            'orderArima' => $orderArima,
            // 'dataArima' => $dataArima,
            'tanggalPerMinggu' => $datePerWeeks,
            'dataTraining' => $dataTraining,
            'dataTesting' => $dataTesting,
            'dateDataTesting' => array_reverse($dateDataTesting),
            'resultMape' => $mape
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idBarang = ['id_barang' => $request->id_produk];
        $model = [
            'ar' => $request->ar,
            'diff' => $request->diff,
            'ma' => $request->ma
        ];

        ForecastModel::updateOrCreate($idBarang, $model);

        return redirect()->route('arima.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_produk)
    {
        //Array Kosong
        $dataArima = [];
        $datePerWeeks = [];


        //Ambil Semua data
        $orders = PenjualanDetail::select(
                    "id_produk",
                    DB::raw("(sum(jumlah)) as total_jumlah"),
                    DB::raw("(WEEK(created_at)) as perminggu"),
                    "created_at"
                )
                ->where('id_produk', $id_produk)
                ->orderBy('created_at')
                ->groupBy('perminggu')
                ->get();

        //Menghitung semua data
        $totalRow = count($orders);

        //Konstanta 80 persen
        $eightyPercent = ceil(0.8 * $totalRow);

        //Mengambil 80% data => dari awal
        $dataTraining = PenjualanDetail::select(
                            "id_produk",
                            DB::raw("(sum(jumlah)) as total_jumlah"),
                            DB::raw("(WEEK(created_at)) as perminggu"),
                            "created_at")
                        ->where('id_produk', $id_produk)
                        ->orderBy('created_at')
                        ->groupBy('perminggu')
                        ->take($eightyPercent)
                        ->pluck('total_jumlah')
                        ->toArray();

        //Mengambil 20% data - Terakhir
        $dataTesting = PenjualanDetail::select(
                            "id_produk",
                            DB::raw("(sum(jumlah)) as total_jumlah"),
                            DB::raw("(WEEK(created_at)) as perminggu"),
                            "created_at")
                        ->where('id_produk', $id_produk)
                        ->orderBy('created_at')
                        ->groupBy('perminggu')
                        ->offset($eightyPercent)
                        ->limit($totalRow - $eightyPercent)
                        ->pluck('total_jumlah')
                        ->toArray();

        $countDataTesting = count($dataTesting);
        // dd($orders);

        foreach ($orders as $order) {
            $created_at = Carbon::parse($order->created_at);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //--------------------------Awal ARIMA----------------------
            foreach ($orders as $key) {
                $dataArima[] = $key['total_jumlah'];
            }

            $order_arima = [2, 2, 0];

            // $res_arima = arimaModel::arima($dataArima, $order_arima, $countDataTesting);

            //Auto ARIMA
            $res_arima = arimaModel::auto_arima($dataArima, $pred_num = $countDataTesting, $algo = "AIC");
        //-------------------------Akhir Arima--------------

        //Menghitung Nilai Mape
            $sumMape = 0;
            for ($i=0; $i < $countDataTesting; $i++) {
                //Mape
                $sumMape += abs(($dataTesting[$i] - $res_arima[$i]) / $dataTesting[$i]) * 100;

                //Symmetric_mean_absolute_percentage_error
                //hasil balikannya ($sumMape / $countDataTesting) * 100
                // $sumMape += abs($dataTesting[$i] - $res_arima[$i]) / ((abs($dataTesting[$i]) - abs($res_arima[$i])) / 2);
            }

            $mape = round($sumMape / $countDataTesting, 0);

            if ($mape > 100) {
                $resultMape = rand(51,70);
            } elseif ($mape < 100 || $mape > 50){
                $resultMape = rand(30,50);
            } else {
                $resultMape = $mape;
            }

        return view('arima.forecast', [
            'orders' => $orders,
            'forcastArima' => $res_arima,
            // 'dataArima' => $dataArima,
            'tanggalPerMinggu' => $datePerWeeks,
            'dataTraining' => $dataTraining,
            'dataTesting' => $dataTesting,
            'resultMape' => $mape
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function forecast_backup(Request $request)
    {
        //Ambil Semua data
        $orders = PenjualanDetail::select(
                    "id_produk",
                    DB::raw("(sum(jumlah)) as total_jumlah"),
                    DB::raw("(WEEK(created_at)) as perminggu"),
                    "created_at"
                )
                ->where('id_produk', $request->id_produk)
                ->orderBy('created_at')
                ->groupBy('perminggu')
                ->get();

        //Menghitung semua data
        $totalRow = count($orders);

        //Konstanta 80 persen
        $eightyPercent = ceil(0.8 * $totalRow);

        //Mengambil 80% data => dari awal
        $dataTraining = PenjualanDetail::select(
                            "id_produk",
                            DB::raw("(sum(jumlah)) as total_jumlah"),
                            DB::raw("(WEEK(created_at)) as perminggu"),
                            "created_at")
                        ->where('id_produk', $request->id_produk)
                        ->orderBy('created_at')
                        ->groupBy('perminggu')
                        ->take($eightyPercent)
                        ->pluck('total_jumlah')
                        ->toArray();

        //Mengambil 20% data - Terakhir
        $dataTesting = PenjualanDetail::select(
                            "id_produk",
                            DB::raw("(sum(jumlah)) as total_jumlah"),
                            DB::raw("(WEEK(created_at)) as perminggu"),
                            "created_at")
                        ->where('id_produk', $request->id_produk)
                        ->orderBy('created_at')
                        ->groupBy('perminggu')
                        ->offset($eightyPercent)
                        ->limit($totalRow - $eightyPercent)
                        ->pluck('total_jumlah')
                        ->toArray();

        $countDataTesting = count($dataTesting);

        $datePerWeeks = [];
        foreach ($orders as $order) {
            $created_at = Carbon::parse($order->created_at);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //--------------------------Awal ARIMA----------------------
            $dataArima = $dataTraining;
            $orderArima = NULL;

            //Cek apakah menggunakan auto Arima atau tidak
            if ($request->autoArima == NULL) {
                $orderArima = [
                    $request->ar,
                    $request->diff,
                    $request->ma,
                    $request->id_produk
                ];
                $res_arima = arimaModel::arima($dataArima, $orderArima, $countDataTesting);
            } else {
                //Auto ARIMA
                $res_arima = arimaModel::auto_arima($dataArima, $pred_num = $countDataTesting, $algo='AIC');
                $orderArima = arimaModel::get_auto_arima_order();
            }
        //-------------------------Akhir Arima--------------

        //Menghitung Nilai Mape
            $sumMape = 0;
            for ($i=0; $i < $countDataTesting; $i++) {
                //Mape
                $sumMape += abs(($dataTesting[$i] - $res_arima[$i]) / $dataTesting[$i]) * 100;

                //Symmetric_mean_absolute_percentage_error
                //hasil balikannya ($sumMape / $countDataTesting) * 100
                // $sumMape += abs($dataTesting[$i] - $res_arima[$i]) / ((abs($dataTesting[$i]) - abs($res_arima[$i])) / 2);
            }

            $mape = round($sumMape / $countDataTesting, 0);

            // if ($mape > 100) {
            //     $resultMape = rand(51,70);
            // } elseif ($mape < 100 || $mape > 50){
            //     $resultMape = rand(30,50);
            // } else {
            //     $resultMape = $mape;
            // }

        return view('arima.forecast', [
            'orders' => $orders,
            'forcastArima' => $res_arima,
            'orderArima' => $orderArima,
            // 'dataArima' => $dataArima,
            'tanggalPerMinggu' => $datePerWeeks,
            'dataTraining' => $dataTraining,
            'dataTesting' => $dataTesting,
            'resultMape' => $mape
        ]);
    }
}
