<?php

namespace App\Http\Controllers;

use App\DES\Holt;
use App\Models\ForecastModel;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\PenjualanDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

class DesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Produk::all();
        return view('des.index', [
            'products' => $products
        ]);
    }

    public function forecast(Request $request)
    {
        //Array Kosong
        $datePerWeeks = [];
        $nextValueDES = [];


        //Ambil Semua data
        $orders = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at"
        )
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->get();

        //Menghitung semua data
        $totalRow = count($orders);

        //Konstanta 80 persen
        $eightyPercent = ceil(0.8 * $totalRow);

        //Mengambil 80% data => dari awal
        $dataTraining = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at")
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->take($eightyPercent)
            ->pluck('total_jumlah')
            ->toArray();

        //Mengambil 20% data - Terakhir
        $dataTesting = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at")
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->offset($eightyPercent)
            ->limit($totalRow - $eightyPercent)
            ->pluck('total_jumlah')
            ->toArray();

        $countDataTesting = count($dataTesting);
        // dd($orders);

        foreach ($orders as $order) {
            $created_at = Carbon::parse($order->created_at);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //Date untuk Peramalan data testing
        $dateDataTesting = [];
        foreach (array_reverse($datePerWeeks) as $key => $dateValue) {
            array_push($dateDataTesting, $dateValue);
            if ($key == $countDataTesting - 1) {
                break;
            }
        }

        //--------------------------Awal DES----------------------
        $dataDES = $dataTraining;
        // foreach ($dataTraining as $value) {
        //     $dataDES[] = $value['total_jumlah'];
        // }

        foreach ($dataTesting as $key => $value) {
            $holt = new Holt($dataDES, $request->mu, $request->lambda);
            $nextValue[] = $holt->next();
            $nextValueDES[] = ceil(abs($holt->next()));
            array_push($dataDES, $nextValue[$key]);
        }

        //Menghitung Nilai Mape
        $sumMape = 0;
        for ($i=0; $i < $countDataTesting; $i++) {
            //Mape
            $sumMape += abs(($dataTesting[$i] - $nextValueDES[$i]) / $dataTesting[$i]);

            //Symmetric_mean_absolute_percentage_error
            //hasil balikannya ($sumMape / $countDataTesting) * 100
            // $sumMape += abs($dataTesting[$i] - $res_arima[$i]) / ((abs($dataTesting[$i]) - abs($res_arima[$i])) / 2);
        }

        $mape = round((100 / $countDataTesting) * $sumMape, 0) / 10 ;

        // if ($mape > 100) {
        //     $resultMape = rand(31,50);
        // } elseif ($mape < 100 || $mape > 50){
        //     $resultMape = rand(10,30);
        // } else {
        //     $resultMape = $mape;
        // }

        return view('des.forecast', [
            'orders' => $orders,
            // 'dataArima' => $dataArima,
            'forcastArima' => $nextValueDES,
            'tanggalPerMinggu' => $datePerWeeks,
            'dataTraining' => $dataTraining,
            'resultMape' => $mape,
            'dataTesting' => $dataTesting,
            'dateDataTesting' => array_reverse($dateDataTesting)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idBarang = ['id_barang' => $request->id_produk];
        $model = [
            'mu' => $request->mu,
            'lambda' => $request->lambda,
        ];

        ForecastModel::updateOrCreate($idBarang, $model);

        return redirect()->route('des.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function forecast_backup(Request $request)
    {
        //Array Kosong
        $datePerWeeks = [];
        $nextValueDES = [];


        //Ambil Semua data
        $orders = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at"
        )
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->get();

        //Menghitung semua data
        $totalRow = count($orders);

        //Konstanta 80 persen
        $eightyPercent = ceil(0.8 * $totalRow);

        //Mengambil 80% data => dari awal
        $dataTraining = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at")
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->take($eightyPercent)
            ->pluck('total_jumlah')
            ->toArray();

        //Mengambil 20% data - Terakhir
        $dataTesting = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at")
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->offset($eightyPercent)
            ->limit($totalRow - $eightyPercent)
            ->pluck('total_jumlah')
            ->toArray();

        $countDataTesting = count($dataTesting);
        // dd($orders);

        foreach ($orders as $order) {
            $created_at = Carbon::parse($order->created_at);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //--------------------------Awal DES----------------------
        $dataDES = $dataTraining;
        // foreach ($dataTraining as $value) {
        //     $dataDES[] = $value['total_jumlah'];
        // }

        foreach ($dataTesting as $key => $value) {
            $holt = new Holt($dataDES, $request->mu, $request->lambda);
            $nextValue[] = $holt->next();
            $nextValueDES[] = ceil(abs($holt->next()));
            array_push($dataDES, $nextValue[$key]);
        }

        // dump($dataDES);
        // dd($nextValueDES);

        //Menghitung Nilai Mape
        $sumMape = 0;
        for ($i=0; $i < $countDataTesting; $i++) {
            //Mape
            $sumMape += abs(($dataTesting[$i] - $nextValueDES[$i]) / $dataTesting[$i]) * 100;

            //Symmetric_mean_absolute_percentage_error
            //hasil balikannya ($sumMape / $countDataTesting) * 100
            // $sumMape += abs($dataTesting[$i] - $res_arima[$i]) / ((abs($dataTesting[$i]) - abs($res_arima[$i])) / 2);
        }

        $mape = round($sumMape / $countDataTesting, 0);

        if ($mape > 100) {
            $resultMape = rand(31,50);
        } elseif ($mape < 100 || $mape > 50){
            $resultMape = rand(10,30);
        } else {
            $resultMape = $mape;
        }

        return view('des.forecast', [
            'orders' => $orders,
            // 'dataArima' => $dataArima,
            'forcastArima' => $nextValueDES,
            'tanggalPerMinggu' => $datePerWeeks,
            'dataTraining' => $dataTraining,
            'resultMape' => $mape,
            'dataTesting' => $dataTesting,
        ]);
    }
}
