<?php

namespace App\Http\Controllers;

use App\ARIMA\arimaModel;
use App\DES\Holt;
use App\Models\ForecastModel;
use App\Models\PenjualanDetail;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ForecastController extends Controller
{
    public function index(){
        $products = ForecastModel::whereNotNull('ar')
                    ->whereNotNull('mu')
                    ->get();
        return view('implementation.index', [
            'products' => $products
        ]);
    }

    public function forecast(Request $request)
    {
        // Function call with passing the start date and end date
        $dates = getDatesFromRange($request->start, $request->end);

        //Find Start and End week
        foreach ($dates as $date) {
            $created_at = Carbon::parse($date);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //grouping Week
        $weekGroups = array();
        $nextValueDES = array();
        foreach ($datePerWeeks as $key => $week){
            if (!in_array($week, $weekGroups)){
                array_push($weekGroups, $week);
            }

            //DES
//            $holt = new Holt($dataDES, $request->mu, $request->lambda);
//            $nextValue[] = $holt->next();
//            $nextValueDES[] = ceil(abs($holt->next()));
//            array_push($dataDES, $nextValue[$key]);
        }

        //Get Model Algoritma
        $models = ForecastModel::where('id_barang', $request->id_produk)->first();

        //Ambil Semua data
        $namaProduk = Produk::select('nama_produk')
            ->where('id_produk', $request->id_produk)
            ->first();
        $arimaData = PenjualanDetail::select(
                        "id_produk",
                        DB::raw("(sum(jumlah)) as total_jumlah"),
                        DB::raw("(WEEK(created_at)) as perminggu"),
                        "created_at")
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->pluck('total_jumlah')
            ->toArray();

        //Arima
        $orderArima = [
            $models->ar,
            $models->diff,
            $models->ma
        ];

        $res_arima = arimaModel::arima($arimaData, $orderArima, count($weekGroups));


        //Des
        $nextValueDES = [];
        for ($i=0; $i < count($weekGroups)  ; $i++) {
            $holt = new Holt($arimaData, $models->mu, $models->lambda);
            $nextValue[] = $holt->next();
            $nextValueDES[] = ceil(abs($holt->next()));
        }

        return view('implementation.forecast')->with(
            compact(
                'weekGroups',
                'res_arima',
                'nextValueDES',
                'namaProduk'
            )
        );
    }

    public function implementationIndex(){
        $products = ForecastModel::whereNotNull('ar')
            ->whereNotNull('mu')
            ->get();
        return view('implementation.implementationIndex', [
            'products' => $products
        ]);
    }
    public function implementation(Request $request)
    {
        // Function call with passing the start date and end date
        $dates = getDatesFromRange($request->start, $request->end);

        //Find Start and End week
        foreach ($dates as $date) {
            $created_at = Carbon::parse($date);
            $start = $created_at->startOfWeek()->format('d/m/Y');
            $end = $created_at->endOfWeek()->format('d/m/Y');
            $week = "$start - $end";

            $datePerWeeks[] = "$week";
        }

        //grouping Week
        $weekGroups = array();
        $nextValueDES = array();
        foreach ($datePerWeeks as $key => $week){
            if (!in_array($week, $weekGroups)){
                array_push($weekGroups, $week);
            }

            //DES
//            $holt = new Holt($dataDES, $request->mu, $request->lambda);
//            $nextValue[] = $holt->next();
//            $nextValueDES[] = ceil(abs($holt->next()));
//            array_push($dataDES, $nextValue[$key]);
        }

        //Get Model Algoritma
        $models = ForecastModel::where('id_barang', $request->id_produk)->first();

        //Ambil Semua data
        $namaProduk = Produk::select('nama_produk')
            ->where('id_produk', $request->id_produk)
            ->first();
        $arimaData = PenjualanDetail::select(
            "id_produk",
            DB::raw("(sum(jumlah)) as total_jumlah"),
            DB::raw("(WEEK(created_at)) as perminggu"),
            "created_at")
            ->where('id_produk', $request->id_produk)
            ->orderBy('created_at')
            ->groupBy('perminggu')
            ->pluck('total_jumlah')
            ->toArray();

        //Arima
        $orderArima = [
            $models->ar,
            $models->diff,
            $models->ma
        ];

        $res_arima = arimaModel::arima($arimaData, $orderArima, count($weekGroups));

        return view('implementation.implementation')->with(
            compact(
                'weekGroups',
                'res_arima',
                'namaProduk'
            )
        );
    }

    public function AllModel()
    {
        $products = Produk::all();

        foreach ($products as $product){
            $idBarang = [
                'id_barang' => $product->id_produk
            ];
            $model = [
                'ar' => 2,
                'diff' => 4,
                'ma' => 1,
                'mu' => 0.50,
                'lambda' => 0.70
            ];

            ForecastModel::updateOrCreate($idBarang, $model);
        }

        return redirect()->route('dashboard');
    }
}
