<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForecastModel extends Model
{
    use HasFactory;

    protected $table = 'forecast_model';
    protected $guarded = [];

    public function getProduk()
    {
        return $this->belongsTo(Produk::class, 'id_barang', 'id_produk');
    }
}
