<?php

namespace App\Imports;

use App\Models\Penjualan;
use App\Models\PenjualanDetail;
use App\Models\Produk;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportPenjualanDetail implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $minProduk = Produk::min('id_produk');
        $maxProduk = Produk::max('id_produk');

        $idPenjualan = Penjualan::where('kodePr', '=', $row[0])->first();
        $id_penjualan = empty($idPenjualan)? 0 : $idPenjualan->id_penjualan;

        $idProduk = Produk::where('nama_produk', 'LIKE', '%'. $row[1] . '%')->first();
        $id_produk = empty($idProduk)? rand($minProduk, $maxProduk) : $idProduk->id_produk;

        if (is_numeric($row[2])) {
            $harga_jual = $row[2];
        } else {
            $harga_jual = 0;
        }

        if (is_numeric($harga_jual) && is_numeric($row[3])) {
            $sub_total = ($harga_jual * $row[3]);
          } else {
            $sub_total = 0;
          }


        // dd($id_produk);

        return new PenjualanDetail([
            'id_penjualan' => $id_penjualan,
            'id_produk' => $id_produk,
            'harga_jual' => $harga_jual,
            'jumlah' => $row[3],
            'diskon' => 0,
            'subtotal' => $sub_total,
            'created_at' => $row[4],
        ]);
    }
}
