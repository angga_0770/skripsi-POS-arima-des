<?php

namespace App\Imports;

use DB;
use App\Models\Member;
use App\Models\Penjualan;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportPenjualan implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //Mencari id member dengan nama toko/member
        $namaToko = $row[1];
        $idMember = Member::where('nama', 'LIKE', '%' . $namaToko . '%')->first();

        $randomIdMember = rand(2,799);

        //Validasi id Memmber Jika kosong
        $id_member= empty($idMember)? $randomIdMember : $idMember->id_member;


        return new Penjualan([
            'kodePr' => $row[0],
            'id_member' => $id_member,
            'total_item' => $row[5],
            'total_harga' => $row[2],
            'diskon' => $row[3],
            'bayar' => $row[4],
            'diterima' => $row[4],
            'id_user' => 1,
            'created_at' => $row[6],
            // 'updated_at' => $row[7]
        ]);
    }
}
