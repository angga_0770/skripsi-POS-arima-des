<?php

namespace App\Imports;

use App\Models\Member;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportMember implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $kode_member = tambah_nol_didepan($row[0], 6);
        return new Member([
            'kode_member' => $kode_member,
            'nama' => $row[1],
            'alamat' => $row[3],
            'telepon' => $row[2]
        ]);
    }
}
