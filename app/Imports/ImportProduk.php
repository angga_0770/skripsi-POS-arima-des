<?php

namespace App\Imports;

use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportProduk implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $kategori = Kategori::where('nama_kategori', 'LIKE', '%' . $row[3] . '%')->first();
        // dd($kategori->id_kategori);
        $idKategori = $kategori->id_kategori;

        $kode_produk = 'P'. tambah_nol_didepan((int)$row[4] +1, 6);

        return new Produk([
            'id_kategori' => $idKategori,
            'kode_produk' => $kode_produk,
            'nama_produk' => $row[0],
            'harga_beli' => $row[2],
            'harga_jual' => $row[1],
            'stok' => 100
        ]);
    }
}
