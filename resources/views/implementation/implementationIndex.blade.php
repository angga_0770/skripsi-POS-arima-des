@extends('layouts.master-flat-ui')
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@section('title')
    Implementation Algoritma
@endsection

@section('breadcrumb')
    @parent
    Implementasi Algoritma
@endsection

@section('content')
    <div class="padding">
        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <form action="/implementation/implementation" method="POST" class="form-horizontal mt-4 ml-4">
                        @csrf
                        <input type="hidden" name="id_produk" value="{{ request('id_produk') }}">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="id_produk" class="control-label"><b>Barang</b></label>
                            </div>
                            <div class="col-lg-8">
                                <select name="id_produk" id="id_produk" class="form-control" required>
                                    <option value="">Pilih Barang</option>
                                    @foreach ($products as $key => $produk)
                                        <option value="{{ $produk->id_barang }}">{{ $produk->getProduk->nama_produk }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="id_ar" class="control-label"><b>Start Date : </b></label>
                            </div>
                            <div class="col-lg-8">
                                <div class="start-date date input-group p-0 shadow-sm">
                                    <input type="text" placeholder="Start Date" class="form-control" name="start">
                                    <div class="input-group-append"><span class="input-group-text px-4"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="id_d" class="control-label"><b>End Date</b></label>
                            </div>
                            <div class="col-lg-8">
                                <div class="end-date date input-group p-0 shadow-sm">
                                    <input type="text" placeholder="End Date" class="form-control" name="end">
                                    <div class="input-group-append"><span class="input-group-text px-4"></span></div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mb-4">Forecast</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- @dd($datasetForecast) --}}
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('.start-date').datepicker({
            setDate: new Date(),
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true,
            startDate: '-0m',
            minDate: 0,
            onSelect: function(text, dt) {
                $('.end-date').datepicker('option', 'minDate', text);
            }
        });

        $('.end-date').datepicker({
            //setDate: new Date(),
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true,
            startDate: '+7d',
            minDate: 0,
        });

    </script>
@endpush
