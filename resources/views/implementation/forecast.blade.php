@extends('layouts.master-flat-ui')
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@section('title')
    Implementation Algoritma
@endsection

@section('breadcrumb')
    @parent
    Implementasi Algoritma
@endsection

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-lg-12">
                <h1>Peramalan Penjualan </h1>
                <h4> Barang : {{ $namaProduk->nama_produk }} </h4>
                <div class="box">
                    <div class="box-body table-responsive">
                        <table class="table table-stiped table-bordered table-forecast">
                            <thead>
                                <th width="5%">No</th>
                                <th>Tanggal</th>
                                <th>Nama Produk</th>
                                <th>ARIMA</th>
                                <th>DES</th>
                            </thead>
                            <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @for($i=0; $i < count($weekGroups)  ; $i++)
                                <tr>
                                    <td> {{ $no++ }}</td>
                                    <td> {{ $weekGroups[$i] }} </td>
                                    <td> {{ $namaProduk->nama_produk }} </td>
                                    <td> {{ abs($res_arima[$i]) }} </td>
                                    <td> {{ $nextValueDES[$i] }} </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
