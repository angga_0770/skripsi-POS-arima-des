<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <title>{{ $setting->nama_perusahaan }} | @yield('title')</title>
  <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">


  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="../assets/images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="{{ asset('flatkit-ui/images/logo.png') }}">

  <!-- style -->
  {{-- <link rel="stylesheet" href="../assets/animate.css/animate.min.css" type="text/css" /> --}}
  {{-- <link rel="stylesheet" href="../assets/glyphicons/glyphicons.css" type="text/css" /> --}}
  {{-- <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css" type="text/css" /> --}}
  {{-- <link rel="stylesheet" href="../assets/material-design-icons/material-design-icons.css" type="text/css" /> --}}

  {{-- <link rel="stylesheet" href="../assets/bootstrap/dist/css/bootstrap.min.css" type="text/css" /> --}}
  <!-- build:css ../assets/styles/app.min.css -->
  {{-- <link rel="stylesheet" href="../assets/styles/app.css" type="text/css" /> --}}
  <!-- endbuild -->
  {{-- <link rel="stylesheet" href="../assets/styles/font.css" type="text/css" /> --}}

  <link rel="stylesheet" href="{{ asset('/flatkit-ui/animate.css/animate.min.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/flatkit-ui/glyphicons/glyphicons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/flatkit-ui/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/flatkit-ui/material-design-icons/material-design-icons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/flatkit-ui/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/flatkit-ui/styles/app.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/flatkit-ui/styles/font.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/AdminLTE-2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  @stack('css')
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  @includeIf('layouts.flat-ui.sidebar')
  <!-- / -->

  <!-- content -->
  @includeIf('layouts.flat-ui.header')
    <!-- ############ PAGE START-->

    @yield('content')

    <!-- ############ PAGE END-->

  <!-- / -->

  <!-- theme switcher -->
  @includeIf('layouts.flat-ui.switcher')
  <!-- / -->

<!-- ############ LAYOUT END-->

  </div>
<!-- build:js scripts/app.html.js -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>

<!-- jQuery -->
<script src="{{ asset('/flatkit-ui/libs/jquery/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap -->
<script src=" {{ asset('/flatkit-ui/libs/jquery/tether/dist/js/tether.min.js') }}"></script>
<script src=" {{ asset('/flatkit-ui/libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>


  <!-- core -->
<script src=" {{ asset('/flatkit-ui/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
<script src=" {{ asset('/flatkit-ui/libs/jquery/PACE/pace.min.js') }}"></script>

<script src="{{ asset('/flatkit-ui/scripts/config.lazyload.js') }}"></script>

<script src="{{ asset('/flatkit-ui/scripts/palette.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-load.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-jp.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-include.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-device.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-form.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-nav.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-screenfull.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-scroll-to.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ui-toggle-class.js') }}"></script>

<script src="{{ asset('/flatkit-ui/scripts/app.js') }}"></script>

<!-- ajax -->
<script src="{{ asset('/flatkit-ui/libs/jquery/jquery-pjax/jquery.pjax.js') }}"></script>
<script src="{{ asset('/flatkit-ui/scripts/ajax.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('AdminLTE-2/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('AdminLTE-2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Validator -->
<script src="{{ asset('js/validator.min.js') }}"></script>

{{-- Apex chart --}}
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<!-- endbuild -->
@stack('scripts')
</body>
</html>
