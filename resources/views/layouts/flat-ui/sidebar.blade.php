<div id="aside" class="app-aside modal nav-dropdown">
    <!-- fluid app aside -->
  <div class="left navside dark dk" data-layout="column">
      <div class="navbar no-radius">
      <!-- brand -->
      <a class="navbar-brand">
          <div ui-include="' {{ asset('/flatkit-ui/images/logo.svg') }}'"></div>
          <img src=" {{ asset('/flatkit-ui/images/logo.png') }}" alt="." class="hide">
          <span class="hidden-folded inline">{{ $setting->nama_perusahaan }}</span>
      </a>
      <!-- / brand -->
    </div>
    <div class="hide-scroll" data-flex>
        <nav class="scroll nav-light">

            <ul class="nav" ui-nav>
             @if(Auth::user()->level == 1)
              <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                <a href="{{ route('dashboard') }}">
                  <span class="nav-icon">
                    <i class="material-icons">&#xe3fc;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_0.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Dashboard</span>
                </a>
              </li>
             @endif
              {{-- <li class="nav-header hidden-folded">
                <small class="text-muted">Master</small>
              </li> --}}
                 @if(Auth::user()->level == 1)
              <li>
                <a>
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="material-icons">&#xe5c3;
                        <span ui-include="' {{ asset('/flatkit-ui/images/i_1.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Data</span>
                </a>
                <ul class="nav-sub">
                    <li class="{{ Request::is('kategori') ? 'active' : '' }}">
                        <a href="{{ route('kategori.index') }}" >
                          <span class="nav-icon">
                            <i class="material-icons">&#xe8d2;
                              <span ui-include="' {{ asset('/flatkit-ui/images/i_1.svg') }}'"></span>
                            </i>
                          </span>
                          <span class="nav-text">Kategori</span>
                        </a>
                    </li>
                    <li  class="{{ Request::is('produk') ? 'active' : '' }}">
                        <a href="{{ route('produk.index') }}" >
                          <span class="nav-icon">
                            <i class="material-icons">&#xe8cb;
                              <span ui-include="' {{ asset('/flatkit-ui/images/i_2.svg') }}'"></span>
                            </i>
                          </span>
                          <span class="nav-text">Produk</span>
                        </a>
                    </li>
                    <li  class="{{ Request::is('member') ? 'active' : '' }}">
                        <a href="{{ route('member.index') }}" >
                          <span class="nav-icon">
                            <i class="material-icons">&#xe7fc;
                              <span ui-include="' {{ asset('/flatkit-ui/images/i_3.svg') }}'"></span>
                            </i>
                          </span>
                          <span class="nav-text">Member</span>
                        </a>
                    </li>
                    <li  class="{{ Request::is('supplier') ? 'active' : '' }}">
                        <a href="{{ route('supplier.index') }}" >
                          <span class="nav-icon">
                            <i class="material-icons">&#xe530;
                              <span ui-include="' {{ asset('/flatkit-ui/images/i_4.svg') }}'"></span>
                            </i>
                          </span>
                          <span class="nav-text">Supplier</span>
                        </a>
                    </li>
                </ul>
              </li>
                @endif


              <li class="nav-header hidden-folded">
                <small class="text-muted">Transaksi</small>
              </li>
                 @if(Auth::user()->level == 1)
              <li  class="{{ Request::is('pengeluaran') ? 'active' : '' }}">
                <a href="{{ route('pengeluaran.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8d2;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_5.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Pengeluaran</span>
                </a>
              </li>
              <li  class="{{ Request::is('pembelian') ? 'active' : '' }}">
                <a href="{{ route('pembelian.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8d2;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_6.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Pembelian</span>
                </a>
              </li>
              <li  class="{{ Request::is('penjualan') ? 'active' : '' }}">
                <a href="{{ route('penjualan.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8d2;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_7.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Penjualan</span>
                </a>
              </li>
                 @endif
              <li  class="{{ Request::is('transaksi') ? 'active' : '' }}">
                <a href="{{ route('transaksi.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8d2;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_8.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Transaksi Aktif</span>
                </a>
              </li>
              <li  class="{{ Request::is('transaksi') ? 'active' : '' }}">
                <a href="{{ route('transaksi.baru') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8d2;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_a.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Transaksi Baru</span>
                </a>
              </li>

                 @if(Auth::user()->level == 1)
              <li class="nav-header hidden-folded">
                <small class="text-muted">Forecasting</small>
              </li>
              <li class="{{ Request::is('arima*') ? 'active' : '' }}">
                <a href="{{ route('arima.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe24b;
                        <span ui-include="' {{ asset('/flatkit-ui/images/i_c.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">ARIMA</span>
                </a>
              </li>
              <li class="{{ Request::is('des*') ? 'active' : '' }}">
                <a href="{{ route('des.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8e5;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_c.svg') }}'"></span>
                    </i>
                  </span>
                  <span class="nav-text">DES</span>
                </a>
              </li>
                <li class="{{ Request::is('implementation*') ? 'active' : '' }}">
                    <a href="{{ route('implementation.index') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe8e5;
                      <span ui-include="' {{ asset('/flatkit-ui/images/i_c.svg') }}'"></span>
                    </i>
                  </span>
                        <span class="nav-text">Forecasting</span>
                    </a>
                </li>

{{--                     <li class="{{ Request::is('implementation*') ? 'active' : '' }}">--}}
{{--                         <a href="{{ url('implementation/implementation') }}" >--}}
{{--                              <span class="nav-icon">--}}
{{--                                <i class="material-icons">&#xe8e5;--}}
{{--                                  <span ui-include="' {{ asset('/flatkit-ui/images/i_c.svg') }}'"></span>--}}
{{--                                </i>--}}
{{--                              </span>--}}
{{--                             <span class="nav-text">Implementation</span>--}}
{{--                         </a>--}}
{{--                     </li>--}}



              <li class="nav-header hidden-folded">
                <small class="text-muted">Report</small>
              </li>

              <li class="hidden-folded" >
                <a href="{{ route('laporan.index') }}">
                    <span class="nav-icon">
                        <i class="material-icons">&#xe8d2;
                          <span ui-include="' {{ asset('/flatkit-ui/images/i_c.svg') }}'"></span>
                        </i>
                    </span>
                  <span class="nav-text">Laporan</span>
                </a>
              </li>
                 @endif
            </ul>
        </nav>
    </div>
    <div class="b-t">
      <div class="nav-fold">
          <a href="{{ route('user.profil') }}">
                <span class="pull-left">
                    <img src="{{ url(auth()->user()->foto ?? '') }}" alt="..." class="w-40 img-circle">
                </span>
              <span class="clear hidden-folded p-x">
                <span class="block _500">{{ auth()->user()->name }}</span>
                <small class="block text-muted"><i class="fa fa-circle text-success m-r-sm"></i>online</small>
              </span>
          </a>
      </div>
    </div>
  </div>
</div>
