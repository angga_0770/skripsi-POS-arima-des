<div id="content" class="app-content box-shadow-z0" role="main">
    <div class="app-header white box-shadow">
        <div class="navbar navbar-toggleable-sm flex-row align-items-center">
            <!-- Open side - Naviation on mobile -->
            <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
                <i class="material-icons">&#xe5d2;</i>
            </a>

            <span class="label label-md dark pos-rlt m-r-xs">
                @section('breadcrumb')
                    Home /
                @show
            </span>


            <!-- navbar right -->
            <ul class="nav navbar-nav ml-auto flex-row">
                <li class="nav-item dropdown pos-stc-xs">
                    <a class="nav-link mr-2" href data-toggle="dropdown">
                        {{ auth()->user()->name }}
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link p-0 clear" href="#" data-toggle="dropdown" onclick="$('#logout-form').submit()">
                    <span class="avatar w-32">
                        <img src="{{ url(auth()->user()->foto ?? '') }}" alt="...">
                        <i class="on b-white bottom"></i>
                    </span>
                    </a>
                </li>
            </ul>
            <form action="{{ route('logout') }}" method="post" id="logout-form" style="display: none;">
                @csrf
            </form>
            <!-- / navbar right -->
        </div>
    </div>
    <div class="app-footer">
        <div class="p-2 text-xs">
          <div class="pull-right text-muted py-1">
            &copy; Copyright <strong>Retailkita</strong> <span class="hidden-xs-down"> - Built with ❤</span>
            <a ui-scroll-to="content"><i class="fa fa-long-arrow-up p-x-sm"></i></a>
          </div>
        </div>
    </div>
<div ui-view class="app-body" id="view">
