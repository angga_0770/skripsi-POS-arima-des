@extends('layouts.master-flat-ui')

@section('title')
    Dashboard
@endsection

@section('breadcrumb')
    @parent
    Dashboard
@endsection

@section('content')
<div class="p-a white lt box-shadow">
	<div class="row">
		<div class="col-sm-6">
			<h4 class="mb-0 _300">Welcome to {{ auth()->user()->name }}</h4>
		</div>
		</div>
	</div>
</div>
<!-- Small boxes (Stat box) -->
<div class="padding">
    <div class="row">
        <div class="col-xs-12 col-sm-3">
	        <div class="box p-a">
	          <div class="pull-left m-r">
	            <span class="w-48 rounded  accent">
	              <i class="material-icons">&#xe164;</i>
	            </span>
	          </div>
	          <div class="clear">
	            <h4 class="m-0 text-lg _300"><a href> {{ $kategori }} <span class="text-sm">Total Kategori</span></a></h4>
	            <small class="text-muted">
                    <a href="{{ route('kategori.index') }}" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
                </small>
	          </div>
	        </div>
	    </div>
        <div class="col-xs-12 col-sm-3">
	        <div class="box p-a">
	          <div class="pull-left m-r">
	            <span class="w-48 rounded blue">
	              <i class="material-icons">&#xe8cb;</i>
	            </span>
	          </div>
	          <div class="clear">
	            <h4 class="m-0 text-lg _300"><a href> {{ $produk }} <span class="text-sm">Total Produk</span></a></h4>
	            <small class="text-muted">
                    <a href="{{ route('produk.index') }}" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
                </small>
	          </div>
	        </div>
	    </div>
        <!-- ./col -->
        <div class="col-xs-12 col-sm-3">
	        <div class="box p-a">
	          <div class="pull-left m-r">
	            <span class="w-48 rounded green">
	              <i class="material-icons">&#xe7fc;</i>
	            </span>
	          </div>
	          <div class="clear">
	            <h4 class="m-0 text-lg _300"><a href> {{ $member }} <span class="text-sm">Total Member</span></a></h4>
	            <small class="text-muted">
                    <a href="{{ route('member.index') }}" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
                </small>
	          </div>
	        </div>
	    </div>
        <!-- ./col -->
        <div class="col-xs-12 col-sm-3">
	        <div class="box p-a">
	          <div class="pull-left m-r">
	            <span class="w-48 rounded red">
	              <i class="material-icons">&#xe530;</i>
	            </span>
	          </div>
	          <div class="clear">
	            <h4 class="m-0 text-lg _300"><a href> {{ $supplier }} <span class="text-sm">Total Supplier</span></a></h4>
	            <small class="text-muted">
                    <a href="{{ route('supplier.index') }}" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
                </small>
	          </div>
	        </div>
	    </div>
        <!-- ./col -->
    </div>

    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Pendapatan {{ tanggal_indonesia($tanggal_awal, false) }} s/d {{ tanggal_indonesia($tanggal_akhir, false) }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="height: 180px;"></canvas>
                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
<!-- /.row (main row) -->
</div>
@endsection

@push('scripts')
<!-- ChartJS -->
<script src="{{ asset('AdminLTE-2/bower_components/chart.js/Chart.js') }}"></script>
<script>
$(function() {
    // Get context with jQuery - using jQuery's .get() method.
    var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
    // This will get the first returned node in the jQuery collection.
    var salesChart = new Chart(salesChartCanvas);

    var salesChartData = {
        labels: {{ json_encode($data_tanggal) }},
        datasets: [
            {
                label: 'Pendapatan',
                fillColor           : 'rgba(60,141,188,0.9)',
                strokeColor         : 'rgba(60,141,188,0.8)',
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: {{ json_encode($data_pendapatan) }}
            }
        ]
    };

    var salesChartOptions = {
        pointDot : false,
        responsive : true
    };

    salesChart.Line(salesChartData, salesChartOptions);
});
</script>
@endpush
