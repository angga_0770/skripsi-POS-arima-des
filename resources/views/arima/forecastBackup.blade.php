@extends('layouts.master-flat-ui')

@section('title')
    Algoritma ARIMA
@endsection

@section('breadcrumb')
    @parent
    Algoritma ARIMA
@endsection

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div id="chart" height="100"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="box">
                    <form action="/arima/forecast" method="POST" class="form">
                        @csrf
                        <div class="box-body p-v-md">
                            <input type="hidden" name="id_produk" value="{{ request('id_produk') }}">
                            <div class="row">
                                <div class="col-xs-4 col-md-4">
                                    <label for="" class="form-control-label"> AR (p) : </label>
                                    <input type="text" class="form-control mb-2" name="ar"
                                        value="{{ request('ar') }}">
                                </div>
                                <div class="col-xs-4 col-md-4">
                                    <label for="" class="form-control-label"> Diff (d) : </label>
                                    <input type="text" class="form-control mb-2" name="diff"
                                        value="{{ request('diff') }}">
                                </div>
                                <div class="col-xs-4 col-md-4">
                                    <label for="" class="form-control-label"> MA (q) : </label>
                                    <input type="text" class="form-control mb-2" name="ma"
                                        value="{{ request('ma') }}">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-10">&nbsp;</div>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn btn-primary col-lg-12">Forecast</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form action="/arima/forecastModel" method="POST">
                    @csrf
                    <input type="hidden" name="id_produk" value="{{ request('id_produk') }}">
                    <input type="hidden" name="algo" value="arima">
                    <input type="hidden" name="ar" value="{{ request('ar') }}">
                    <input type="hidden" name="diff" value="{{ request('diff') }}">
                    <input type="hidden" name="ma" value="{{ request('ma') }}">
                    <button type="submit" class="btn btn-success col-lg-12">Save Model</button>
                </form>
            </div>
        </div>
    </div>
    <div class="padding">
        <div class="row">
            <div class="col-lg-6">
                <h1>Data Penjualan Barang</h1>
                <div class="box">
                    <div class="box-body table-responsive">
                        <table class="table table-stiped table-bordered table-pembelian">
                            <thead>
                                <th width="5%">No</th>
                                <th>Tanggal</th>
                                <th>Nama Produk</th>
                                {{-- <th>Minggu Ke - </th> --}}
                                <th>Jumlah</th>
                            </thead>
                            <tbody>

                                @foreach ($orders as $key => $order)
                                    <tr>
                                        <td> {{ $loop->iteration }}</td>
                                        <td> {{ $tanggalPerMinggu[$key] }}</td>
                                        <td> {{ $order->produk->nama_produk }} </td>
                                        {{-- <td> {{ $order->perminggu }} </td> --}}
                                        <td> {{ $order->total_jumlah }} </td>
                                    </tr>
                                    @php
                                        //Mengambil Tanggal Terakhir Iterasi
                                        if ($loop->last) {
                                            $lastTanggal = $tanggalPerMinggu[$key];
                                            $namaProduk = $order->produk->nama_produk;
                                        }
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @php
                //Memisahkan Tanggal awal dan Akhir
                $explodeTanggalTerakhir = explode('-', $lastTanggal);

                //Mengganti tanda / menjadi -
                $dateReplace = str_replace('/', '-', $explodeTanggalTerakhir[1]);

                //Mengubah Format d-m-y menjadi Y-m-d
                $dateFormatLastDate = date('Y-m-d', strtotime($dateReplace));
            @endphp
            @php
                //Join DataTraining(NULL) - Data Testing = DatasetJoin
                $datasetJoin = [];

                //Join DatasetJoin(NULL) - Forecasting
                $datasetForecast = [];
            @endphp

            {{-- Menggabungkan dataTraining, Data Testing, dan data Forecasting untuk kebutuhan CHART --}}
            @foreach ($dataTraining as $key => $nullValue)
                @php
                    $datasetJoin[] = null;
                    if ($loop->last) {
                        $datasetJoin[$key] = $nullValue;
                    }
                @endphp
            @endforeach
            @foreach ($dataTesting as $key => $value)
                @php
                    array_push($datasetJoin, $value);
                @endphp
            @endforeach
            @foreach ($dataTraining as $key => $allNULL)
                @php
                    $datasetForecast[] = null;
                    if ($loop->last) {
                        $datasetForecast[$key] = $allNULL;
                    }
                @endphp
            @endforeach
            {{-- END Menggabungkan dataTraining, Data Testing, dan data Forecasting untuk kebutuhan CHART --}}

            {{-- Menambahkan Tanggal untuk Forecast --}}
            @php
                $dateForecast = [];
                $dateForecastEnd = [];
                array_push($dateForecastEnd, $dateFormatLastDate);
            @endphp
            {{--}}@foreach ($dataTesting as $key => $DT)
                @php
                    if ($loop->first) {
                        $forecastDate = \Carbon\Carbon::parse($dateForecastEnd[0]);
                        $start2 = $forecastDate
                            ->addDay(1)
                            ->startOfWeek()
                            ->format('d/m/Y');
                        $end2 = $forecastDate
                            ->addDay(1)
                            ->endOfWeek()
                            ->format('d/m/Y');
                        $week2 = "$start2 - $end2";

                        $endDateForecast = $forecastDate
                            ->addDay(1)
                            ->endOfWeek()
                            ->format('Y-m-d');
                        array_push($dateForecastEnd, "$endDateForecast");
                        array_push($dateForecast, "$week2");
                    } else {
                        $forecastDate = \Carbon\Carbon::parse($dateForecastEnd[$key]);
                        $start2 = $forecastDate->startOfWeek()->format('d/m/Y');
                        $end2 = $forecastDate->endOfWeek()->format('d/m/Y');
                        $week2 = "$start2 - $end2";

                        $endDateForecast = $forecastDate
                            ->addDay(1)
                            ->endOfWeek()
                            ->format('Y-m-d');
                        array_push($dateForecastEnd, "$endDateForecast");
                        array_push($dateForecast, "$week2");
                    }
                @endphp
            @endforeach --}}
            {{-- @dd($dateForecast) --}}
            <div class="col-lg-6">
                <h1>Peramalan ARIMA</h1>
                <div class="box">
                    <div class="box-body table-responsive">
                        <table class="table table-stiped table-bordered table-forecast">
                            <thead>
                                <th width="5%">No</th>
                                <th>Tanggal</th>
                                <th>Nama Produk</th>
                                <th>Aktual</th>
                                <th>Peramalan</th>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                {{-- @for ($i = 0; $i < count($forcastArima); $i++)
                                    @php
                                        $dateForecastWeeks = $dateForecast[$i];
                                        $valueForecast = abs($forcastArima[$i]);

                                        array_push($tanggalPerMinggu, $dateForecastWeeks);
                                        array_push($datasetForecast, "$valueForecast");
                                    @endphp
                                    <tr>
                                        <td> {{ $no++ }}</td>
                                        <td>{{ $dateForecast[$i] }}</td>
                                        <td> {{ $namaProduk }}</td>
                                        <td>{{ $valueForecast }}</td>
                                    </tr>
                                @endfor --}}
                                @dd($dataTesting)
                                @foreach ($dataTesting as $testing)
                                    <tr>
                                        <td> {{ $no++ }}</td>
                                        <td>{{ $dateForecast[$i] }}</td>
                                        <td> {{ $namaProduk }}</td>
                                        <td>{{ $valueForecast }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr class="active">
                                    <td colspan="2" class="text-right"> Mean absolute percentage error (MAPE) : </td>
                                    <td class="text-right">
                                        <b> {{ $resultMape }} % </b>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- @dd($datasetForecast) --}}
@push('scripts')
    <script>
        let table_pembelian;

        table3 = $('.table-pembelian').DataTable({
            "searching": false,
            stateSave: true
        });

        //Apex Chart
        var options = {
            series: [{
                    name: 'Data Training',
                    data: {!! json_encode($dataTraining) !!}
                },
                {
                    name: 'Data Testing',
                    data: {!! json_encode($datasetJoin) !!}
                },
                {
                    name: 'Forecast',
                    data: {!! json_encode($datasetForecast) !!}
                }
            ],
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                    enabled: false
                },
                animations: {
                    enabled: false
                }
            },
            stroke: {
                width: [5, 5, 4],
                curve: 'straight'
            },
            labels: {!! json_encode($tanggalPerMinggu) !!},
            title: {
                text: ''
            },
            xaxis: {},
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();

        // //---------------------------------------------------------------------
        // //Canva JS
        // const labels = {!! json_encode($tanggalPerMinggu) !!};

        // const values = {{-- json_encode($forcastArima) --}};
        // const values = {{-- json_encode($dataArima) --}};
        // const namaProduk = "{{ $namaProduk }}"
        // // console.log(namaProduk);
        // const data = {
        // labels: labels,
        // datasets: [
        //     {
        //         label: 'Data Penjualan - ' + namaProduk ,
        //         backgroundColor: 'rgb(255, 99, 132)',
        //         borderColor: 'rgb(255, 99, 132)',
        //         data: values,
        //     }
        // ]
        // };

        // const config = {
        //     type: 'line',
        //     data: data,
        //     options: {
        //         responsive: true,
        //         interaction: {
        //             mode: 'index',
        //             intersect: false,
        //         }
        //     }
        // };

        // const myChart = new Chart(
        //     document.getElementById('myChart'),
        //     config
        // );
    </script>
@endpush
