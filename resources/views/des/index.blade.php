@extends('layouts.master-flat-ui')

@section('title')
    Algoritma DES
@endsection

@section('breadcrumb')
    @parent
    Algoritma DES
@endsection

@section('content')
<div class="padding">
    <div class="row">
        <div class="box">
            <div class="col-lg-12">
                <form action="/des/forecast" method="POST" class="form-horizontal mt-4 ml-4">
                    @csrf
                    <input type="hidden" name="id_produk" value="{{ request('id_produk') }}">
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label for="id_produk" class="control-label"><b>Barang</b></label>
                        </div>
                        <div class="col-lg-8">
                            <select name="id_produk" id="id_produk" class="form-control" required>
                                <option value="">Pilih Barang</option>
                                @foreach ($products as $key => $produk)
                                    <option value="{{ $produk->id_produk }}">{{ $produk->nama_produk }}</option>
                                @endforeach
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label for="id_ar" class="control-label"><b>Alpha</b></label>
                        </div>
                        <div class="col-lg-8">
                            <input type="number" class="form-control mb-3" placeholder="Masukkan MU" id="mu" name="mu" required step="0.1">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label for="id_d" class="control-label"><b>Beta </b></label>
                        </div>
                        <div class="col-lg-8">
                            <input type="number" class="form-control mb-3" placeholder="Masukkan lambda" id="lambda" name="lambda" required step="0.1">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-4">Forecast</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- @dd($datasetForecast) --}}
@push('scripts')
<script>
    const arText = document.getElementById("ar")
    const iText = document.getElementById("diff")
    const maText = document.getElementById("ma")

    $('#autoArima').change(function() {
        const checked = document.querySelector('#autoArima:checked') !== null;

        if (checked) {
            arText.disabled = true
            iText.disabled = true
            maText.disabled = true
        } else {
            arText.disabled = false
            iText.disabled = false
            maText.disabled = false
        }
    });

</script>
@endpush
